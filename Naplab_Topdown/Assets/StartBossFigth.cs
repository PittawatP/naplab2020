﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBossFigth : MonoBehaviour
{
    public Animator amt;
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            amt.SetBool("Start", true);
            Destroy(this.gameObject);
        }
    }
}
