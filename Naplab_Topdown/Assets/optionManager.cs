﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using MoreMountains.Feedbacks;

namespace MoreMountains.TopDownEngine
{
    public class optionManager : MonoBehaviour
    {
        public Toggle OnMusic;
        public Slider musicValue;
        public Toggle OnSFX;
        public Slider SFXValue;
        // Start is called before the first frame update
        private void Start()
        {
            if (GameObject.FindObjectOfType<SoundManager>() == null)
            {
                return;
            }
            SoundManager SM = GameObject.FindObjectOfType<SoundManager>().GetComponent<SoundManager>();
            OnMusic.isOn = SM.Settings.MusicOn;
            OnSFX.isOn = SM.Settings.SfxOn;
            musicValue.value = SM.MusicVolume;
            SFXValue.value = SM.SfxVolume;   
        }                   
        // Update is called once per frame
        void Update()
        {
            if (GameObject.FindObjectOfType<SoundManager>()==null)
            {
                return;
            }
            SoundManager SM = GameObject.FindObjectOfType<SoundManager>().GetComponent<SoundManager>();

            SM.Settings.MusicOn = OnMusic.isOn;
            SM.Settings.SfxOn = OnSFX.isOn;
            SM.MusicVolume = musicValue.value;
            SM.SfxVolume = SFXValue.value;
        } 
    }
}

