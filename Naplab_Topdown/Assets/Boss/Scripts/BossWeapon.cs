﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.TopDownEngine
{
	public class BossWeapon : MonoBehaviour
	{
		public int attackDamage = 10;
		public int enragedAttackDamage = 40;

		public Vector3 attackOffset;
		public float attackRange = 1f;
		public LayerMask attackMask;
		public GameObject prefabbullet;
		private Projectile pjt;
		public GameObject point;
        private void Update()
        {
			//if (health != null)
			//    return;
			//health = GameObject.FindObjectOfType<Health>();
		pjt=prefabbullet.GetComponent<Projectile>();

		}


        public void Attack()
		{
			Vector3 pos = transform.position;
			pos += transform.right * attackOffset.x;
			pos += transform.up * attackOffset.y;

			Collider2D colInfo = Physics2D.OverlapCircle(pos, attackRange, attackMask);
			if (colInfo != null)
			{
				colInfo.GetComponent<Health>().Damage(attackDamage, colInfo.gameObject, 2,0);
			}
		}

		public void EnragedAttack()
		{
			Vector3 pos = transform.position;
			pos += transform.right * attackOffset.x;
			pos += transform.up * attackOffset.y;

			Collider2D colInfo = Physics2D.OverlapCircle(pos, attackRange, attackMask);
			if (colInfo != null)
			{
				colInfo.GetComponent<PlayerHealth>().TakeDamage(enragedAttackDamage);
			}
		}

		public void ShootBullet()
        {
			Vector3 newpost =new Vector3(  GameObject.FindGameObjectWithTag("Player").transform.position.x - point.transform.position.x,
				 GameObject.FindGameObjectWithTag("Player").transform.position.y- point.transform.position.y);

		 pjt.SetDirection((newpost).normalized,
                Quaternion.identity,false);
			Instantiate(prefabbullet, point.transform.position, Quaternion.identity);
			pjt.Movement();
          
            AudioManager.instance.PlaySound("Shoot");
		}

		void OnDrawGizmosSelected()
		{
			Vector3 pos = transform.position;
			pos += transform.right * attackOffset.x;
			pos += transform.up * attackOffset.y;

			Gizmos.DrawWireSphere(pos, attackRange);
		}

	}
}