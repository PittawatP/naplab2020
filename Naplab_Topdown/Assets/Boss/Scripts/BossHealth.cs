using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;

	public class BossHealth : MonoBehaviour
	{

		public int health;
		private int _health;
		public GameObject deathEffect;
	public int TakenDMG;
	public GameObject panel;
		public bool isInvulnerable = false;

		private void Start()
		{
			_health = health;

		}
		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.P))
			{
				TakeDamage(100);
				Debug.Log(_health);
			}
		}
		public void TakeDamage(int damage)
		{
			if (isInvulnerable)
				return;
		AudioManager.instance.PlaySound("TakeDMG");
		_health -= damage;

			if (_health <= (75 * health / 100))
			{
				GetComponent<Animator>().SetBool("stage1", true);

			}
			if (_health <= (60 * health / 100))
			{
				GetComponent<Animator>().SetBool("stage2", true);
			}
			if (_health <= (50 * health / 100))
			{
				GetComponent<Animator>().SetBool("stage3", true);
				GetComponent<Animator>().SetBool("jump", true);
			}
			if (_health <= (30 * health / 100))
			{
				GetComponent<Animator>().SetBool("stage4", true);
			}

			if (_health <= 0)
			{
				Die();
			}
		Debug.Log(_health);

			//switch (health) { 
			//	case 200:
			//		GetComponent<Animator>().SetBool("IsEnraged", true);
			//		break;
			//	case 0: Die();
			//		break;
			//}
		}

		void Die()
		{
			Instantiate(deathEffect, transform.position, Quaternion.identity);
			GetComponent<Animator>().SetBool("dead", true);
		panel.SetActive(true);
			//Destroy(gameObject);
		}

        private void OnTriggerEnter2D(Collider2D collision)
        {
			TakeDamage(TakenDMG);

		}

    }
