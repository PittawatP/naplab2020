﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MoreMountains.TopDownEngine
{
    public class Boss_jump : StateMachineBehaviour
    {
        Boss boss;
        Transform transform;
        GameObject go;
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            boss = animator.GetComponent<Boss>();
            transform = GameObject.FindGameObjectWithTag("Boss").transform;
            animator.GetComponent<BossHealth>().isInvulnerable = true;
            boss.gameObject.GetComponent<BoxCollider2D>().isTrigger = true ;
            boss.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition;
            go = Instantiate(boss.spawnkiki, transform.position, Quaternion.identity);


        }


        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {


            if (go.GetComponent<Health>().CurrentHealth<=0)
            {
                animator.SetBool("jump", false);
                animator.SetBool("jumpfall", true);
            }
        }


        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.GetComponent<BossHealth>().isInvulnerable = false;
            boss.gameObject.GetComponent<BoxCollider2D>().isTrigger = false;
            boss.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            boss.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;

        }

        // OnStateMove is called right after Animator.OnAnimatorMove()
        //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        //{
        //    // Implement code that processes and affects root motion
        //}

        // OnStateIK is called right after Animator.OnAnimatorIK()
        //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        //{
        //    // Implement code that sets up animation IK (inverse kinematics)
        //}
    }
}