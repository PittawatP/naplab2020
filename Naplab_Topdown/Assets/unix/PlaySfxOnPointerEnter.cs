﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlaySfxOnPointerEnter : MonoBehaviour
{
    public AudioClip Sfx;
public void PlaySfx()
    {
        GetComponent<AudioSource>().PlayOneShot(Sfx);
    }
}
