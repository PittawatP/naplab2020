﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MoreMountains.TopDownEngine
{
    public class Picked : MonoBehaviour
    {
        public bool isPicked = false;
        public int index;
        private void Awake()
        {
          isPicked =GameManager.Instance.itemRemmember[index];
        }
        private void Start()
        {
            if (isPicked)
            {
                this.gameObject.SetActive(false);
            }
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            isPicked = true;
            GameManager.Instance.itemRemmember[index] = isPicked; 
        }

    }
}