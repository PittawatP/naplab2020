﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MoreMountains.TopDownEngine
{
    [System.Serializable]
    public class PlayerData
    {
        public int level;
        public int health;
        public float[] position;
        public int[] LevelStat;
        public bool[] itemRemmember;

        public PlayerData(Player player)
        {
            level = player.level;
            health = player.health;

            position = new float[3];
            position[0] = player.Playerposition.x;
            position[1] = player.Playerposition.y;
            position[2] = player.Playerposition.z;
            LevelStat = new int[3];
            LevelStat[0] = GameManager.Instance.Status[0];
            LevelStat[1] = GameManager.Instance.Status[1];
            LevelStat[2] = GameManager.Instance.Status[2];
            itemRemmember = new bool[32];

            for (int i = 0; i <GameManager.Instance.itemRemmember.Length ; i++)
            {
                itemRemmember[i] = GameManager.Instance.itemRemmember[i];
            }
        }
    }
}
