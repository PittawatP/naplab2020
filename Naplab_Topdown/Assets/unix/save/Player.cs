﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace MoreMountains.TopDownEngine
{
    public class Player : MonoBehaviour
    {
        public int level;
        public int health;
        public Vector3 Playerposition;
        public GameObject player;
        public void Awake()
        {
            LoadPlayer();
        }
        public void Update()
        {if (GameObject.FindGameObjectWithTag("Player") == null)
                return;



            player = GameObject.FindGameObjectWithTag("Player");
            level = SceneManager.GetActiveScene().buildIndex;
            health = player.GetComponent<Health>().CurrentHealth;
            Playerposition = player.transform.position;

       
        }
     
        public void LoadPlayer()
        {
            PlayerData data = SaveSystem.LoadPlayer();
            if (data == null)
                return;

            level = data.level;
            health = data.health;
            Vector3 position;
            position.x = data.position[0];
            position.y = data.position[1];
            position.z = data.position[2];
            Playerposition = position;

            
            GameManager.Instance.position = Playerposition;
            GameManager.Instance.Health = health;
            GameManager.Instance.LV = level;

            if(data.LevelStat!=null)
            for(int i = 0; i < data.LevelStat.Length; i++)
            {
                GameManager.Instance.Status[i] =data.LevelStat[i] ;
            }

            for (int i = 0; i < GameManager.Instance.itemRemmember.Length; i++)
            {
                GameManager.Instance.itemRemmember[i] = data.itemRemmember[i];
            }
            Debug.Log("Loading");
        }
    } }
