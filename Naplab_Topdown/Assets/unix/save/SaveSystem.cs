﻿using System.IO;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
namespace MoreMountains.TopDownEngine
{
    public static class SaveSystem
    {
        public static void SavePlayer(Player player)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            string path = Application.persistentDataPath + "/player.Dun";
            FileStream stream = new FileStream(path, FileMode.Create);

            PlayerData data = new PlayerData(player);

            formatter.Serialize(stream, data);
            stream.Close();
        }

        public static PlayerData LoadPlayer()
        {
            string path = Application.persistentDataPath + "/player.Dun";
            if (File.Exists(path))
            {
                BinaryFormatter formatter = new BinaryFormatter();

                FileStream stream = new FileStream(path, FileMode.Open);
                PlayerData data = formatter.Deserialize(stream) as PlayerData;

                stream.Close();
                return data;
            }
            else
            {
                Debug.LogError("Save file not found in " + path);
                return null;
            }
        }
        public static void DeleteSave()
        {
            string path = Application.persistentDataPath + "/player.Dun";
            if (File.Exists(path))
            {
                File.Delete(path);
                GameManager.Instance.ResetSave();
            }
            else
            {
                Debug.LogError("Save file not found in " + path);
               
            }
        }
    }
}