﻿using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    public Sound[] Theme;
    public static AudioManager instance;
    // Start is called before the first frame update
    void Awake()
    {if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        foreach(Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.picth;
            s.source.spatialBlend = s.Spiralblen;
            s.source.loop = s.loop;
        }
        foreach (Sound t in Theme)
        {
            t.source = gameObject.AddComponent<AudioSource>();
            t.source.clip = t.clip;
            t.source.volume = t.volume;
            t.source.pitch = t.picth;
            t.source.loop = t.loop;
        }
    }
    private void Start()
    {
        Play("theme");
    }
    public void Play(string name)
    {
      Sound s=  Array.Find(Theme, Sound => Sound.name == name);
        if (s == null)
            return;
        s.source.Play();
    }
    public void PlaySound(string name)
    {
        Sound s = Array.Find(sounds, Sound => Sound.name == name);
        if (s == null)
            return;
        s.source.Play();
    }
}
