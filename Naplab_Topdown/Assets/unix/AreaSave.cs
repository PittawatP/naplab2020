﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
public class AreaSave : MonoBehaviour
{
    public GameObject panel;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        string path = Application.persistentDataPath + "/player.Dun";
        if (File.Exists(path))
            return;

            panel.SetActive(true);
    }
    public void setToFalse()
    {
        this.gameObject.SetActive(false);
    }
}
