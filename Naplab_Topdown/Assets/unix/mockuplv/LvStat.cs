﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.Feedbacks;
using UnityEngine.UI;


namespace MoreMountains.TopDownEngine
{
    public class LvStat : MonoBehaviour
    {
        public int LvHp;
        public int LvDash;
        public int LvSpeed;


        public int MaxHpperLv = 20;
        public float DashCDperLv = 0.3f;
        public int SpeedperLv = 5;
        private Health Hp;
        private CharacterDash2D Dash;
        private CharacterRun move;
        public Button UpHp;
        public Button UpDash;
        public Button UpSpeed;

        public Image[] stackHp;
        public Image[] stackDash;
        public Image[] stackSpeed;
        // Start is called before the first frame update
        private void Start()
        {
            LvHp = GameManager.Instance.Status[0];
            LvDash = GameManager.Instance.Status[1];
          LvSpeed=  GameManager.Instance.Status[2] ;
        }
        private void Update()
        {
            
            if (GameObject.FindGameObjectWithTag("Player") == null)
                return;

           
          
           
            if (Hp == null)
            {
                Hp = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
                Hp.MaximumHealth = 30 + (LvHp * MaxHpperLv);
            }
           
            if (Dash == null)
            {
                Dash = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterDash2D>();
                Dash.Cooldown.PauseOnEmptyDuration = 2 - (LvDash * DashCDperLv);
            }
               
            if (move == null)
            {
                move = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterRun>();
                move.RunSpeed = 10 + (LvSpeed * SpeedperLv);
            }
            for (int i = 0; i < LvHp; i++)
            {
                stackHp[i].color = Color.white;
            }
            for (int i = 0; i < LvDash; i++)
            {
                stackDash[i].color = Color.white;
            }
            for (int i = 0; i < LvSpeed; i++)
            {
                stackSpeed[i].color = Color.white;
            }

        }


        public void UpdateHp()
        { 
            LvHp++;
            GameManager.Instance.Status[0] = LvHp;
            if (LvHp >= 3)
            {
                UpHp.interactable = false;
                   LvHp = 3;

            }
            Hp.MaximumHealth = 30 + (LvHp * MaxHpperLv);
            Hp.SetHealth(Hp.MaximumHealth);
          
        }
        public void UpdateDash()
        {
            LvDash++;
            GameManager.Instance.Status[1] = LvDash;

            if (LvDash >= 3)
            {
                UpDash.interactable = false;
                LvDash = 3;

            }
            Dash.Cooldown.PauseOnEmptyDuration = 2 - (LvDash * DashCDperLv);
        }
        public void UpdateSpeed()
        {
            LvSpeed++;
            GameManager.Instance.Status[2] = LvSpeed;
            if (LvSpeed >= 3)
            {
                UpSpeed.interactable = false;
                LvSpeed = 3;

            }
            move.RunSpeed = 10 + (LvSpeed * SpeedperLv);
        }
    }
}
