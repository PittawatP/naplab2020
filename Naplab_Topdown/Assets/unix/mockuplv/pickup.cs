﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MoreMountains.TopDownEngine
{
    public class pickup : MonoBehaviour
    {
        private Canvas CV;
        private void Update()
        { if (CV != null)
                return;
            CV = GameObject.FindGameObjectWithTag("UIPowerUp").GetComponent<Canvas>();

        }
        // Start is called before the first frame update
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                // CV.SetActive(true);
                GameObject.FindGameObjectWithTag("GameManager").GetComponent<uplv>().pickup = this;
                CV.enabled = true;
                Cursor.visible = true;  
            }
        }
        public void Destroythis()
        {
            Cursor.visible = false;
            Destroy(gameObject);
        }
    } }
