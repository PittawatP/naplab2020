﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.TopDownEngine
{
    public class uplv : MonoBehaviour
    {
        public Canvas canvas;
        public pickup pickup;
        private LvStat Char;


        private void Update()
        {
            if (Char != null)
                return;
            Char = GameObject.FindObjectOfType<LvStat>().GetComponent<LvStat>();
        }

        public void UpHp()
        {
          //  Char.LvHp++;
            Char.UpdateHp();
            pickup.Destroythis();
            canvas.enabled=(false);
        }
        public void UpDash()
        {
           // Char.LvDash++;
            Char.UpdateDash();
            pickup.Destroythis();
            canvas.enabled = (false);
        }
        public void UpSpeed()
        {
           // Char.LvSpeed++;
            Char.UpdateSpeed();
            pickup.Destroythis();
            canvas.enabled = (false);
        }
    }
}
