﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using MoreMountains.Tools;
using System.IO;
namespace MoreMountains.TopDownEngine
{
    public class LoadScene : MonoBehaviour
    {
        public Text text;
        public bool isSave = false;
        private void Update()
        {if (text == null)
                return;
            string path = Application.persistentDataPath + "/player.Dun";
            if (File.Exists(path))
            {
                text.text = "Countinue";
                isSave = true;
              
            }
            else
            {
                text.text = "Start";
                isSave = false;
                return;
            }

        }
        public void LoadNextScene(int lv)
        {
            if (!isSave)
            {
                MMSaveLoadManager.DeleteSaveFolder("InventoryEngine/");
                GameManager.Instance.ResetSave();
                SceneManager.LoadScene(lv);

               // LevelManager.Instance.loadSaveToSpawnPlayer();
            }
                
            else
            {
                SceneManager.LoadScene(GameManager.Instance.LV);
            }
        }
        public void LoadSceneNumber(int lv)
        {
           
                SceneManager.LoadScene(lv);

        }
    }
}