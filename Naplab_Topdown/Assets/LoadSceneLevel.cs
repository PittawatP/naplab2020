﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MoreMountains.Tools;

public class LoadSceneLevel : MonoBehaviour
{
    public enum scenelevel {MainMenu,Level_2,Level_3,Level_4,Level_5,Level_6}

    public scenelevel LoadLevel;

    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            MMGameEvent.Trigger("Save");
            LoadingSceneManager.LoadScene(LoadLevel.ToString());
        }
    }

    public void OnClickToMainMenu()
    {
        SceneManager.LoadScene(LoadLevel.ToString());
    }
}
