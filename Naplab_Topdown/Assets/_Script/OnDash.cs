﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MoreMountains.TopDownEngine
{


public class OnDash : MonoBehaviour
{
        public CharacterDash2D Dashcode;
        public Health HPcode;

    void Start()
    {
            Dashcode.GetComponent<CharacterDash2D>();

            HPcode.GetComponent<Health>();
    }

    // Update is called once per frame
    void Update()
    {
    if (!Dashcode._dashing)
            {
                HPcode.Invulnerable = false;
            }
            else
                HPcode.Invulnerable = true;
    }
}
}
